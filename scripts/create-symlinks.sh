#!/bin/bash

projectname=${PWD##*/}
echo $projectname
# create .env if not exist
if [ ! -f .env ]; then
  cp drupaldocker/.env.example .env
  sed -i "s/SITENAME/${projectname}/" .env
  sed -i "s/SITENAME/${projectname}/" .env
  sed -i "s/SITENAME/${projectname}/" .env
fi

# symlink
if [ ! -f docker-compose.yml ]; then
  ln -s drupaldocker/docker-compose.yml .
fi

# symlink
if [ ! -f .gitignore ]; then
  cp drupaldocker/.gitignore.example .gitignore
fi

# generate certs
if [ ! -d drupaldocker/tools/certs ]; then
  mkdir drupaldocker/tools/certs
  openssl req -x509 -nodes -days 999999 -newkey rsa:2048 -keyout drupaldocker/tools/certs/cert.key -out drupaldocker/tools/certs/cert.crt -subj "/C=ES/ST=Madrid/L=Madrid/O=carcheky/OU=carcheky/CN=${projectname}.docker.localhost/emailAddress=carcheky@gmail.com"
fi
