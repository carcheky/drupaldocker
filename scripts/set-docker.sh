#!/bin/bash

if [ ! -d ~/.ssh ]; then
  ssh-keygen
fi

projectname=${PWD##*/}
profilename=minimal

if [ $1 ]; then
  profilename=$1
fi

echo $projectname
# create .env if not exist
if [ ! -f .env ]; then
  cp drupaldocker/configs/docker/.env.example .env
  sed -i "s/SITENAME/${projectname}/" .env
  sed -i "s/SITENAME/${projectname}/" .env
  sed -i "s/SITENAME/${projectname}/" .env
fi

# symlink
if [ ! -f docker-compose.yml ]; then
  cp drupaldocker/configs/docker/docker-compose.yml .
fi 1>/dev/null

# symlink
if [ ! -f .gitignore ]; then
  cp drupaldocker/configs/docker/.gitignore.example .gitignore
fi 1>/dev/null

# generate certs
if [ ! -d drupaldocker/tools/certs ]; then
  mkdir drupaldocker/tools/certs
  openssl req -x509 -nodes -days 999999 -newkey rsa:2048 -keyout drupaldocker/tools/certs/cert.key -out drupaldocker/tools/certs/cert.crt -subj "/C=ES/ST=Madrid/L=Madrid/O=cckdev/OU=cckdev/CN=${projectname}.docker.localhost/emailAddress=cckdev@cckdev.es"
fi 1>/dev/null


docker stop $(docker ps -a -q) > /dev/null
docker-compose down --remove-orphans;
docker-compose up -d --build --remove-orphans;
