#!/bin/bash

# while true; do
#     read -p "Quieres inicializar docker? (y/n)" yn
#     case $yn in
#         [Yy]* ) docker=y; break;;
#         [Nn]* ) docker=n; break;;
#         * ) echo "responde 'y' o 'n'";;
#     esac
# done
# while true; do
#     read -p "Quieres instalar drupal? (y/n)" yn
#     case $yn in
#         [Yy]* ) drupal=y; break;;
#         [Nn]* ) drupal=n; break;;
#         * ) echo "responde 'y' o 'n'";;
#     esac
# done
# while true; do
#     read -p "Quieres configurar vscode? (y/n)" yn
#     case $yn in
#         [Yy]* ) vscode=y; break;;
#         [Nn]* ) vscode=n; break;;
#         * ) echo "responde 'y' o 'n'";;
#     esac
# done
docker=y
drupal=y
vscode=y

if [ $vscode = 'y' ]
then
  echo "==> Instalando vscode..."
  bash drupaldocker/scripts/set-vscode.sh
fi

if [ $docker = 'y' ]
then
  echo "==> Instalando docker..."
  bash drupaldocker/scripts/set-docker.sh
fi

if [ $drupal = 'y' ]
then
  echo "==> Instalando drupal..."
  bash drupaldocker/scripts/set-drupal.sh
fi